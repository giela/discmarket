-- phpMyAdmin SQL Dump
-- version 4.0.4.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 23, 2015 at 01:24 PM
-- Server version: 5.6.13
-- PHP Version: 5.6.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `discmarket`
--
CREATE DATABASE IF NOT EXISTS `discmarket` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `discmarket`;

-- --------------------------------------------------------

--
-- Table structure for table `artiest`
--

CREATE TABLE IF NOT EXISTS `artiest` (
  `artiestid` int(10) NOT NULL AUTO_INCREMENT,
  `artiest` varchar(50) NOT NULL,
  PRIMARY KEY (`artiestid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `artiest`
--

INSERT INTO `artiest` (`artiestid`, `artiest`) VALUES
(1, 'ArtiestName1'),
(2, 'dutchgoon'),
(3, 'LIL B'),
(4, 'dasd'),
(5, 'wdasda'),
(6, 'asdasda'),
(7, 'yunglean'),
(8, 'kendrick lamar');

-- --------------------------------------------------------

--
-- Table structure for table `cdtabel`
--

CREATE TABLE IF NOT EXISTS `cdtabel` (
  `cdid` int(10) NOT NULL AUTO_INCREMENT,
  `titel` varchar(50) NOT NULL,
  `prijs` float NOT NULL,
  `kwaliteit` int(1) NOT NULL,
  `afbeelding` varchar(50) DEFAULT NULL,
  `beschrijving` varchar(500) DEFAULT NULL,
  `gewicht` float DEFAULT NULL,
  `genreid` int(10) NOT NULL,
  `artiestid` int(10) NOT NULL,
  `uitgeverid` int(10) NOT NULL,
  `gebruikerid` int(10) NOT NULL,
  `uploaddatum` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `gereserveerd` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`cdid`),
  KEY `genreid` (`genreid`),
  KEY `genreid_2` (`genreid`,`artiestid`,`uitgeverid`),
  KEY `artiestid` (`artiestid`,`uitgeverid`),
  KEY `uitgeverid` (`uitgeverid`),
  KEY `gebruikerid` (`gebruikerid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `cdtabel`
--

INSERT INTO `cdtabel` (`cdid`, `titel`, `prijs`, `kwaliteit`, `afbeelding`, `beschrijving`, `gewicht`, `genreid`, `artiestid`, `uitgeverid`, `gebruikerid`, `uploaddatum`, `gereserveerd`) VALUES
(4, 'CDTest1', 5, 5, 'Awesome_smiley_face.png', NULL, NULL, 1, 1, 1, 2, '2015-05-19 12:35:04', NULL),
(5, 'CDTest2', 1, 8, 'Awesome_smiley_face.png', NULL, NULL, 2, 1, 1, 1, '2015-06-23 12:56:48', 1),
(6, 'CDTest3', 100, 1, 'Awesome_smiley_face.png', NULL, NULL, 1, 1, 1, 1, '2015-06-23 12:57:16', 1),
(7, 'CDTest4', 3, 5, 'Awesome_smiley_face.png', NULL, NULL, 1, 1, 1, 1, '2015-06-23 12:56:55', 1),
(9, 'Blue Flame', 22, 1, 'Lil_B_The_Based_God_Blue_Flame-front-large.jpg', 'LIl b op zijn best in blue flame album', 22, 6, 3, 3, 3, '2015-05-20 08:00:23', NULL),
(10, 'Red flame', 23, 4, 'Lil_B_The_BasedGod_Red_Flame-front-large.jpg', 'beste rapper in de geschiedenis van goden.', 25, 6, 3, 3, 3, '2015-05-20 08:01:22', NULL),
(11, 'Black flame', 13, 3, 'homepage_large.317c703e.jpg', 'swag swag swag met lil b', 22, 6, 3, 3, 3, '2015-05-20 08:02:12', NULL),
(12, 'Angels exodus', 350, 5, '7f0850d7594dbcdb4dc27f0bda667f98.jpg', 'Based god will never die for his people!', 22, 6, 3, 4, 3, '2015-05-20 08:03:23', NULL),
(13, 'White flame', 13, 5, 'homepage_large.d60234f6.jpg', 'de witte vlam in lil b komt tot leven', 25, 6, 3, 3, 3, '2015-05-20 08:04:38', NULL),
(14, 'Rain in england', 79, 5, 'LilB_cover.jpg', 'Wie anders dan lil b kan muziek maken over regen in een ander land?', 22, 6, 3, 4, 3, '2015-05-20 09:55:58', NULL),
(18, 'unknown death 2002', 25, 4, 'download.jpg', 'yunglean strapping ', 23, 6, 7, 3, 3, '2015-05-20 13:41:41', NULL),
(19, 'unknown memory', 13, 3, 'homepage_large.f0deb63b.jpg', 'dsada', 23, 6, 7, 3, 3, '2015-05-22 09:12:06', NULL),
(20, 'good kid maad city', 333, 4, 'KendrickGKMCDeluxe.jpg', 'dasdasdasd', 54546, 6, 8, 7, 3, '2015-05-20 13:46:21', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `gebruiker`
--

CREATE TABLE IF NOT EXISTS `gebruiker` (
  `gebruikerid` int(10) NOT NULL AUTO_INCREMENT,
  `naam` varchar(50) NOT NULL,
  `wachtwoord` varchar(255) NOT NULL,
  `email` varchar(50) NOT NULL,
  `vmail` varchar(50) NOT NULL,
  `postcode` varchar(6) DEFAULT NULL,
  `woonplaats` varchar(50) NOT NULL,
  `betrouwbaarheid` int(1) DEFAULT NULL,
  `admin` int(1) NOT NULL DEFAULT '0',
  `cdid` int(10) NOT NULL,
  PRIMARY KEY (`gebruikerid`),
  KEY `cdid` (`cdid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `gebruiker`
--

INSERT INTO `gebruiker` (`gebruikerid`, `naam`, `wachtwoord`, `email`, `vmail`, `postcode`, `woonplaats`, `betrouwbaarheid`, `admin`, `cdid`) VALUES
(1, 'GebruikerNaam', '$2y$10$oavGjBxNfGUcCPJbuUWSZOJPRxGwqWMuRrZfqpGfo5Eo1Ypnzzffm', 'hello@world.com', 'jpgJcU1@kleinepartijen.nl', '1234AA', 'Utrecht', NULL, 0, 0),
(2, 'ASD', '$2y$10$Js3YlNLcOSPXlC./kMVcLe5L8UWSl8eF2lPgv5RGjeLna9.pRR5mi', 's@hotmail.com', 'jpgJcU2@kleinepartijen.nl', '1234AZ', 'Amsterdam', NULL, 0, 0),
(3, 'gieldid', '$2y$10$JiG6WJ4hzy3/PwDa2ojhy.lf66y0nUy0SgEuGUPoYT6QcidWoQZWW', 'giel94@hotmail.com', 'jpgJcU3@kleinepartijen.nl', '1622hl', 'Hoorn', NULL, 0, 0),
(7, 'jew', '$2y$10$byjXREnjt5anrHt02fV8HOqtxxKdhf67EOSm3Jl/9N/ejrPQh1Gh2', 'members@kleinepartijen.nl', 'xBOnOkAQix7@kleinepartijen.nl', '1622HL', 'Hoorn (NH)', NULL, 0, 0),
(8, 'ruud', '$2y$10$T0am237JyzMW3/Kz3Ndx2u3r3A.Pbi.a8RdpxonWclv47HLbPSUiG', 'rudolf@nl-host.com', 'esKeMcrysd8@kleinepartijen.nl', '1645PH', 'de goorn', NULL, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `genre`
--

CREATE TABLE IF NOT EXISTS `genre` (
  `genreid` int(10) NOT NULL AUTO_INCREMENT,
  `genre` varchar(50) NOT NULL,
  PRIMARY KEY (`genreid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `genre`
--

INSERT INTO `genre` (`genreid`, `genre`) VALUES
(1, 'Electronic'),
(2, 'Rock'),
(3, 'Pop'),
(4, 'Funk/Soul'),
(5, 'Jazz'),
(6, 'Hip Hop'),
(7, 'Country'),
(8, 'Classical'),
(9, 'Reggae'),
(10, 'Latin'),
(11, 'Blues'),
(12, 'Children''s');

-- --------------------------------------------------------

--
-- Table structure for table `uitgever`
--

CREATE TABLE IF NOT EXISTS `uitgever` (
  `uitgeverid` int(10) NOT NULL AUTO_INCREMENT,
  `uitgever` varchar(50) NOT NULL,
  PRIMARY KEY (`uitgeverid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `uitgever`
--

INSERT INTO `uitgever` (`uitgeverid`, `uitgever`) VALUES
(1, 'UitgeverName1'),
(2, 'pop a molly im sweating'),
(3, 'indie'),
(4, 'Basedgod waterfront'),
(5, 'sdaasda'),
(6, 'sda'),
(7, 'asdasd');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cdtabel`
--
ALTER TABLE `cdtabel`
  ADD CONSTRAINT `cdtabel_ibfk_1` FOREIGN KEY (`genreid`) REFERENCES `genre` (`genreid`),
  ADD CONSTRAINT `cdtabel_ibfk_2` FOREIGN KEY (`artiestid`) REFERENCES `artiest` (`artiestid`),
  ADD CONSTRAINT `cdtabel_ibfk_3` FOREIGN KEY (`uitgeverid`) REFERENCES `uitgever` (`uitgeverid`),
  ADD CONSTRAINT `cdtabel_ibfk_4` FOREIGN KEY (`gebruikerid`) REFERENCES `gebruiker` (`gebruikerid`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
