$(document).ready(function(){
    function loadData(page){                   
        $.ajax
        ({
            type: "POST",
            url: "cart_update.php",
            data: "page="+page,
            success: function(msg)
            {
                $("#container").ajaxComplete(function(event, request, settings)
                {
                    $("#container").html(msg);
                });
            }
        });
    }

    loadData(0);  // For first time page load default results
    $('#container .pagination li.active').live('click',function(){
        var page = $(this).attr('p');
        loadData(page);            
    });
});