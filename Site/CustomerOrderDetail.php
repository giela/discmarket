<?php 
include "include/header.php";
?>	

<head>
	<!--Verwijderen als deze is toegevoegd in style.css-->
	<link rel="stylesheet" type="text/css" href="styles/styleadmin.css">
</head>

<div id="container">
	<div id="orders">
<?php
$i = 0;
if(!isset($_GET['order_id'])){
	header('Location: crash.php');
}
else{
	$OrderID = $_GET['order_id'];
}

	echo "
	<table id='tabel1' class='table-hover' style=' margin-top:40px; '>
		<tr>
			<th>Bestelnummer</th>
			<th>BestelDatum</th>
			<th>Totaal aantal</th>
			<th>Totaalprijs</th>
		</tr>";
$sql1 ="SELECT * FROM orders AS o JOIN aantalprodperorder AS pa ON pa.OrderID = o.OrderID JOIN products AS p ON p.ProductID = pa.ProductID JOIN orderdetail AS od ON od.OrderDetailID = o.OrderDetailID WHERE CustomerID = $CustomerID LIMIT 1" ;
$result = mysqli_query($GLOBALS['con'], $sql1);
while($row = mysqli_fetch_array($result)){
	
$sql="SELECT p.ProductID, pa.Aantal, (pa.Aantal * p.ProductPrice) AS prijs FROM orders as o JOIN aantalprodperorder AS pa ON o.OrderID = pa.OrderID JOIN products AS p ON pa.ProductID = p.ProductID WHERE CustomerID = $CustomerID and pa.OrderID = $OrderID";
$result1=mysqli_query($GLOBALS['con'], $sql);
	while($row1 = mysqli_fetch_array($result1)){
		$i++;
		$totaalaantal[$i] = $row1['Aantal'];
		$prijs[$i] = $row1['prijs'];
	}
	$totaleaantal = array_sum($totaalaantal);
	$prijsaantal = count($prijs);
	$totaleprijs = array_sum($prijs);
	
	echo "<tr class='info' style='text-align: center;'>";
	echo "<td> $OrderID </td>";
	echo "<td>" . $row['OrderDate']. "</td>";
	echo "<td>" . $totaleaantal . "</td>";
	echo "<td>&#8364;". $totaleprijs . "</td>";
	echo "</tr>";

unset($totaalaantal);
unset($prijs);
}		
$sql2 ="SELECT DISTINCT p.ProductName, p.ProductPrice, p.ProductID, ap.Aantal, od.OrderDetail  FROM aantalprodperorder ap JOIN products p ON p.ProductID=ap.ProductID JOIN orders AS o ON o.OrderID = ap.OrderID JOIN orderdetail AS od ON od.OrderDetailID = o.OrderDetailID WHERE ap.OrderID=  $OrderID" ;
$result2 = mysqli_query($GLOBALS['con'], $sql2);
echo "
	<table class='table-hover' id='tabel2'>
		<tr>
			<th>Product</th>
			<th>Aantal</th>
			<th>BestelStatus</th>
		</tr>";

while($row2 = mysqli_fetch_array($result2)){
	echo "<tr class='info'>";
	echo "<td>" . $row2['ProductName']. "<br /> &#8364;".  $row2['ProductPrice']. "</td>";
	echo "<td>" . $row2['Aantal'] . "</td>";
	echo "<td>" . $row2['OrderDetail'] . "</td>";
	echo "</tr>";
}
echo "</table>"

?>
	</div><!--/orders-->
</div><!--/container-->
<?php 
	include "include/footer.php";
?>
</body>
</html>