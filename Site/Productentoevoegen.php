<?php 
	include "include/header.php";
?>
<html>
	<head>
		<title>Webshop infologic</title>
		<link rel="stylesheet" type="text/css" href="styles/style.css">
		<link rel="stylesheet" type="text/css" href="styles/styleadmin.css">
		<link rel="stylesheet" type="text/css" href="styles/styleproducttoevoegen.css">
	</head>
	<body>		
		<div id="container">
			<div id="contentadminpanel">
				<?php
					$sqlcat="SELECT DISTINCT CategoryID, CategoryName FROM categories"; //categorien query
					$resultcat=mysqli_query($GLOBALS["con"], $sqlcat)  or die(mysqli_error($db_name));//categorien ophalen voor de select list 
					if($rowadmin['Admin'] == 0)
					{
						if(isset($CustomerID))
						{
							echo "U heeft geen toegang tot deze pagina! ";
							echo "<a href='home.php'>home</a>";
						}
						else
						{
							echo "U heeft geen toegang tot deze pagina! ";
							echo "<a href='inlogpagina.php'>login</a>";
						}
					}
					else
					{
				?>
				<aside>
					<a href="Productentoevoegen"><div id="pt" class="buttons">Producten toevoegen</div></a>
					<a href="productkiezen"><div id="pk" class="buttons">Producten wijzigen</div></a>
					<a href="Productaanvullen"><div id="pk" class="buttons">Product aanvullen</div></a>
					<a href="productkiezenverwijder"><div id="pv" class="buttons">Producten verwijderen</div></a>
					<a href="adminpanel"><div id="pt" class="buttons">Order status beheer</div></a>
					<a href="accountbeheer"><div id="pt" class="buttons">Account beheer</div></a>
				</aside>
				<div id="admincontainer">
					<div id="titel">
						Producten toevoegen
					</div>
					<div id="filter">
					</div>
					
						<form id="productform" name="productform" method="POST" action="producttoegevoegd.php" enctype="multipart/form-data">
							<div id="orders">
								<div id="label1-1">
									<div class="label1-1">	Productnaam: </div>
									<div class="label1-1">	Categorie: </div>
									<div class="label1-1">	Prijs: </div>
									<div class="label1-1">	Aantal in vooraad: </div>
									<div class="label1-1">	Beschrijving: </div>
								</div>
									
								<div id="box1">
									<div class="box1-1"><input name="productnaam" type="text" id="productnaam" required><br></div>
									<div class="box1-1"><select name= "categorie" id="categorie" required><?php foreach($resultcat as $value){?><option value="<?php echo $value['CategoryID'];?>"><?php echo $value['CategoryName'];?></option><?php } ?></select><br></div>
									<div class="box1-1" style=" margin-top: -10px;"><input name="prijs" type="text" id="prijs" required><br></div>
									<div class="box1-1"><input type="number" name="aantal" id="aantal"min="0" step="1" value="1" required ><br></div>
									<div class="box1-1"><textarea type="text" name="beschrijving" id="beschrijving" required></textarea><br></div>
								</div>
									
								<div id="label2">
									<div class="label2-1">	Specificaties:</div>
									<div class="label2-1" style=" margin-top: 318px;">	Afbeelding: </div>	
									<div class="label2-1" style=" margin-top: 152px">	Video: </div>	
								</div>
									
								<div id="box2">
									<div class="box2-1"><textarea type="text" name="specs" id="specs" required></textarea><br></div>
									<div class="box2-1"><input type="file" name="afbeelding1" id="afbeelding1" accept="image/*"><br></div>
									<div class="box2-1"><input type="file" name="afbeelding2" id="afbeelding2" accept="image/*"><br></div>
									<div class="box2-1"><input type="file" name="afbeelding3" id="afbeelding3" accept="image/*"><br></div>
									<div class="box2-1"><input type="text" name="video" type="text" id="video" placeholder="URL alleen van youtube.com"><br></div>
								</div>
							</div>
							<div id="minifooter">
								<input type="submit" value="Voeg product toe" name="submit">
							</div>
						</form>	
							
				</div>
				<?php } mysqli_close($GLOBALS['con']); ?>
			</div>
			<div class="push">  </div>
		</div>
	<?php
		include "include/footer.php";
	?>
	</body>
</html>