<?php 
	include "include/header.php";
?>
<html>
	<head>
		<title>Webshop infologic</title>
		<link rel="stylesheet" type="text/css" href="styles/style.css">
		<link rel="stylesheet" type="text/css" href="styles/styleadmin.css">
		<link rel="stylesheet" type="text/css" href="styles/styleproducttoevoegen.css">
	</head>
	<body>		
		<div id="container">
			<div id="contentadminpanel">
				<?php
					$sqlcat="SELECT DISTINCT genreid, genre FROM genre"; //categorien query
					$resultcat=mysqli_query($GLOBALS["con"], $sqlcat)  or die(mysqli_error($GLOBALS["con"]));//categorien ophalen voor de select list 
						if(!isset($CustomerID))
						{
							echo "U heeft geen toegang tot deze pagina! ";
							echo "<a href='inlogpagina.php'>login</a>";
						}
						else
						{
				?>
				<aside>
					<a href="Productentoevoegenverkoper"><div id="pt" class="buttons">Verkopen</div></a>
					<a href="productkiezen"><div id="pk" class="buttons">Producten wijzigen</div></a>
					<a href="productkiezenverwijder"><div id="pv" class="buttons">Producten verwijderen</div></a>
				</aside>
				<div id="admincontainer">
					<div id="titel">
						Producten toevoegen
					</div>
					<div id="filter">
					</div>
						<form id="productform" name="productform" method="POST" action="producttoegevoegdverkoper" enctype="multipart/form-data">
							<div id="orders">
								<div id="label1-1">
								<div class="label1-1">	titel: </div>
								<div class="label1-1">	genre: </div>
								<div class="label1-1">	Prijs in euro: </div>
								<div class="label1-1">	kwaliteit: </div>
								<div class="label1-1">	Beschrijving: </div>
							</div>
							
							<div id="box1">
								<div class="box1-1"><input name="titel" type="text" required></div>
								<div class="box1-1"><select name= "genre" id="genre" required><?php foreach($resultcat as $value){?><option value="<?php echo $value['genreid'];?>"><?php echo $value['genre'];?></option><?php } ?></select></div>
								<div class="box1-1"><input name="prijs" type="text" id="prijs" required></div>
								<div class="box1-1"><input type="number" name="kwaliteit" id="kwaliteit" min="0" max="5" step="1" required ></div>
								<div class="box1-1"><textarea type="text" name="beschrijving" id="beschrijving" required></textarea></div>
							</div>
							
							<div id="label2">
								<div class="label2-1">	Gewicht:</div>
								<div class="label2-1">	artiest: </div>	
								<div class="label2-1">	uitgever: </div>	
								<div class="label2-1">	Afbeelding: </div>	
							</div>
							
							<div id="box2">
								<div class="box2-1"><input type="text" name="gewicht" id="gewicht"  required></div>
								<div class="box2-1"><input type="text" name="artiest" type="text"  ></div>
								<div class="box2-1"><input type="text" name="uitgever" type="text" id="uitgever" ></div>
								<div class="box2-1"><input type="file" name="afbeelding1" id="afbeelding1" accept="image/*" ></div>
								<div class="box2-1"><input type="hidden" name="gebruikerid" id="gebruikerid" value="<?php echo $CustomerID ?>"></div>
							</div>	
							</div>
							<div id="minifooter">
								<input type="submit" value="Verkopen!" name="submit">
							</div>
						</form>	
							
				</div>
				<?php } mysqli_close($GLOBALS['con']); ?>
			</div>
			<div class="push">  </div>
		</div>
	<?php
		include "include/footer.php";
	?>
	</body>
</html>