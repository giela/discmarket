<?php
include "include/header.php";
?>
<?php
requestid();
if(isset($CustomerID)){
	admincheck($host, $username, $password, $db_name, $tbl_name, $CustomerID);
	$sql="SELECT Email FROM $tbl_name WHERE CustomerID='$CustomerID'"; 
	$result=mysqli_query($GLOBALS["con"], $sql); 
	$row = mysqli_fetch_array($result);
}
else{
	$rowadmin['Admin'] = 0;
}
?>
	<div id="container">
		<div id="registratieveld" class="col-xs-8 col-sm-10 col-md-12 col-lg-12">		
			<form action="Registreren.php" method="post" class="form-horizontal">
				<div id="persoonlijkegegevens" class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<h3 class="col-xs-offset-2 col-sm-offset-2 col-md-offset-3 col-lg-offset-3">Registratie</h3>
					<label id="verplicht">Velden met een * zijn verplicht</label><br><br>
						
						<div class="form-group">
							<label id="label" class="col-xs-3 col-sm-3 col-md-5 col-lg-5 control-label">*Naam:</label>
							<div class="col-sm-7">
								<input type="text" class="form-control" name="Naam" required><br>
							</div>
						</div>
						
						<div class="form-group">
							<label id="label" class="col-xs-3 col-sm-3 col-md-5 col-lg-5 control-label">*Postcode:</label>
							<div class="col-sm-7">
								<input type="text" class="form-control" name="Postcode" placeholder="1234AZ" pattern="[0-9]{4}[A-Z]{2}"><br>
							</div>
						</div>

						<div class="form-group">
							<label id="label" class="col-xs-3 col-sm-3 col-md-5 col-lg-5 control-label">*Woonplaats:</label>
							<div class="col-sm-7">
								<input type="text" class="form-control" name="Woonplaats"><br>
							</div>
						</div>
										
					<div class="form-group">
						<label id="label" class="col-xs-3 col-sm-3 col-md-5 col-lg-5 control-label" style="min-width: 130px;" >*E-mailadres:</label>
						<div class="col-sm-7">
							<input type="email" class="form-control" name="EmailAdress" id="email1" required><br>
						</div>
					</div>
					
					<div class="form-group">
						<label id="label" class="col-xs-3 col-sm-3 col-md-5 col-lg-5 control-label" style="min-width: 130px;" >*E-mailadres Controleren:</label>
						<div class="col-sm-7">
							<input type="email" class="form-control" name="EmailAdress2" id="email2" required><br>
						</div>
					</div>
					
					<div class="form-group">
						<label id="label" class="col-xs-3 col-sm-3 col-md-5 col-lg-5 control-label" style="min-width: 130px;" >*Wachtwoord: </label>
						<div class="col-sm-7">
							<input type="password" class="form-control" name="Wachtwoord" id="wachtwoord1"  required><br>
						</div>
					</div>
					
					<div class="form-group">
						<label id="label" class="col-xs-3 col-sm-3 col-md-5 col-lg-5 control-label" style="min-width: 130px;" >*Wachtwoord bevestigen: </label>
						<div class="col-sm-7">
							<input type="password" class="form-control" name="Wachtwoord2" id="wachtwoord2" required><br>
							
						</div>						
					</div>
					</div>

						<div id="checkboxalgemenevoorwaarden">
							<div class='g-recaptcha' data-sitekey='6LfiUgMTAAAAAICOONiYkCYCNh6mtCCIumH4SUWz'></div>
							<input id="algemenevoorwaarden" type="checkbox" required> Ik ga akkoord met de <a href="algemenevoorwaarden.html">algemene voorwaarden</a>	
						</div>
						<input id="buttonregistreren" class="btn btn-default" type="submit" name="submit"  value="registreer" href="home.html">
				</div>
		</div>
				
		<script type="text/javascript">
			window.onload = function () {
				document.getElementById("wachtwoord1").onchange = validatePassword;
				document.getElementById("wachtwoord2").onchange = validatePassword;
				document.getElementById("email1").onchange = validateEmail;
				document.getElementById("email2").onchange = validateEmail;
			}
			function validatePassword(){
			var ww2=document.getElementById("wachtwoord2").value;
			var ww1=document.getElementById("wachtwoord1").value;
			if(ww1!=ww2)
				document.getElementById("wachtwoord2").setCustomValidity("wachtwoorden komen niet overeen");
			else
			document.getElementById("wachtwoord2").setCustomValidity(''); } 
			
			function validateEmail(){
			var em2=document.getElementById("email2").value;
			var em1=document.getElementById("email1").value;
			if(em1!=em2)
				document.getElementById("email2").setCustomValidity("E-mailadressen komen niet overeen");
			else
			document.getElementById("email2").setCustomValidity('');  
		}
		</script>	
	</div>
<?php
include "include/footer.php";
?>		
</body>
</html>	