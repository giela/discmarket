
<?php 
include "include/header.php";
?>

<head> 
	<!--Verwijderen als deze is toegevoegd in style.css-->
	<link rel="stylesheet" type="text/css" href="styles/styleadmin.css">
</head>
		
<div id="container">
<div id="contentadminpanel">
<?php
if($rowadmin['Admin'] == 0)
{
	if(isset($CustomerID))
	{
		echo "U heeft geen toegang tot deze pagina! ";
		echo "<a href='home'>home</a>";
	}
	else
	{
		echo "U heeft geen toegang tot deze pagina! ";
		echo "<a href='inlogpagina.php'>login</a>";
	}
}
else{ 
?>
	<script>
		function showCustomers(str) {
			if (str == "") {
				document.getElementById("orders").innerHTML = "";
				return;
			} else { 
				if (window.XMLHttpRequest) {
					// code for IE7+, Firefox, Chrome, Opera, Safari
					xmlhttp = new XMLHttpRequest();
				} else {
					// code for IE6, IE5
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				}
				xmlhttp.onreadystatechange = function() {
					if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
						document.getElementById("orders").innerHTML = xmlhttp.responseText;
					}
				}
				xmlhttp.open("GET","getcustomer.php?q="+str,true);
				xmlhttp.send();
			}
		}
	</script>
<aside>
	<a href="Productentoevoegen.php"><div id="pt" class="buttons">Producten toevoegen</div></a>
	<a href="productkiezen.php"><div id="pk" class="buttons">Producten wijzigen</div></a>
	<a href="Productaanvullen"><div id="pk" class="buttons">Product aanvullen</div></a>
	<a href="productkiezenverwijder.php"><div id="pv" class="buttons">Producten verwijderen</div></a>
	<a href="adminpanel.php"><div id="pt" class="buttons">Order status beheer</div></a>
	<a href="accountbeheer.php"><div id="pt" class="buttons">Account beheer</div></a>
</aside>
<div id="admincontainer">
	<div id="titel">
		Account beheer
	</div>
	<div id="filter">
		<form id="form">
			Sorteer:
			<select name="Orderstatus" id="orderstatusselect" onchange="showCustomers(this.value)">
				<option value="0">Nieuw/Oud</option>
				<option value="1"selected> Oud/nieuw</option>
			</select>
			<label id="aantalselect">
				Aantal per pagina:
			</label>
			<select name="aantal">
				<option value="10">10</option>
				<option value="20">20</option>
				<option value="30">30</option>
			</select>
		</form>
	</div>
	<form id='verwijderklant' method='post' name='verwijderklant' action='verwijderklant.php'>
	<div id="orders">
<?php 
			//$sql="SELECT c.CustomerID, c.UserName, c.Datecreated, c.LastOnline, COUNT(o.OrderID) as Bestellingen FROM customers c JOIN orders o ON c.CustomerID=o.CustomerID ORDER BY Datecreated DESC"; 
			$sql="SELECT c.CustomerID, c.UserName, c.Datecreated, c.LastOnline FROM customers c ORDER BY Datecreated DESC"; 
			$result = mysqli_query($GLOBALS['con'], $sql);		
			echo "<table>
			<tr>
			<th>CustomerID</th>
			<th></th>
			<th>Gebruikers naam</th>
			<th>Aangemaakt op</th>
			<th>Laatst ingelogd</th>
			</tr>";
			while($row = mysqli_fetch_array($result)) {
				echo "<tr>";
				echo "<td><a href='customerdetail.php?customer_id=" .$row['CustomerID'] ."'>" . $row['CustomerID'] . "</a></td>";
				echo "<td id='check'><input type='checkbox' name='check_list[".$row['CustomerID']."]' value=".$row['CustomerID']."></td>";
				echo "<td>" . $row['UserName']."</td>";
				echo "<td>" . $row['Datecreated'] . "</td>";
				echo "<td>" . $row['LastOnline'] . "</td>";
				echo "</tr>";
			}
			echo "</table>";
?>
	</div><!--/orders-->
	<div id="minifooter"> 
		<input type="submit" name="submit" value="verwijder" onclick="return confirm('Are you sure?');"/>
	</div>
	</form>
</div><!--/admincontainer-->
<?php 
} // End Else
mysqli_close($GLOBALS['con']); 
?>
</div><!--/contentadminpanel-->
</div><!--/container-->
<?php
include "include/footer.php";
?>
</body>
</html>