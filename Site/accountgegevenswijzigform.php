<?PHP
include "include/header.php";
requestid();
dbconnect($host, $username, $password, $db_name);

	if(isset($CustomerID))
	{
		$sqlcust="SELECT * FROM gebruiker WHERE gebruikerid ='$CustomerID'";

		$resultcust=mysqli_query($GLOBALS["con"], $sqlcust)  or die(mysqli_error($db_name));
		$rowcust= $resultcust->fetch_array(MYSQLI_ASSOC); 
	}
	else {
		header("Location:inlogpagina");
	}
?>
<head>
	<!--Verwijderen als deze is toegevoegd in style.css-->
	<link rel="stylesheet" type="text/css" href="styles/accountwijzigstyle.css">
</head>

<form class="form-horizontal" id="form" action="accountgegevenswijzigen.php" method="post" style="min-height: 530px; ">
  	<div class="form-group">
  		<label class="col-sm-offset-2 col-sm-2">E-mailadres wijzigen</label>
  	</div> 

  	<div class="form-group">
		<label for="inputEmail1" class="col-sm-2 control-label">Oud E-mailadres</label>
		<div class="col-sm-2">
            <input type="email" class="form-control" placeholder="Oud E-mailadres" name="OudEmail" id="OudEmail" required>
		</div>
  	</div> 
  	<div class="form-group">
		<label for="inputEmail2" class="col-sm-2 control-label">Nieuw E-mailadres</label>
		<div class="col-sm-2">
            <input type="email" class="form-control" placeholder="Nieuw E-mailadres" name="NieuwEmail" id="email1" >
		</div>
		<div class="col-sm-2">
            <input type="email" class="form-control" placeholder="E-mailadres Bevestigen" name="BevestigEmail" id="email2" >
		</div>  			
	</div> 			   			
	<div class="form-group">
  		<label class="col-sm-offset-2 col-sm-2">Wachtwoord wijzigen</label>
  	</div> 
  	<div class="form-group">
		<label for="inputPassword1" class="col-sm-2 control-label">Oud wachtwoord</label>
		<div class="col-sm-2">
			<input type="password" class="form-control" placeholder="Oud wachtwoord" name="OudWachtwoord" id="OudWachtwoord" required>
		</div>
  	</div> 
  	<div class="form-group">
		<label for="inputPassword2" class="col-sm-2 control-label">Nieuw wachtwoord</label>
		<div class="col-sm-2">
			<input type="password" class="form-control" placeholder="Nieuw wachtwoord" name="NieuwWachtwoord" id="wachtwoord1" >
		</div>
		<div class="col-sm-2">
			<input type="password" class="form-control" placeholder="Wachtwoord Bevestigen" name="BevestigWachtwoord" id="wachtwoord2" >
		</div>  			
	</div>
	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-2">
			<button type="submit" class="btn btn-default">Wijzig</button>
		</div>	
	</div>		 			   			
</form>  			
		
<script type="text/javascript">
	window.onload = function () {
		document.getElementById("wachtwoord1").onchange = validatePassword;
		document.getElementById("wachtwoord2").onchange = validatePassword;
		document.getElementById("email1").onchange = validateEmail;
		document.getElementById("email2").onchange = validateEmail;
	}
	function validatePassword(){
	var ww2=document.getElementById("wachtwoord2").value;
	var ww1=document.getElementById("wachtwoord1").value;
	if(ww1!=ww2)
		document.getElementById("wachtwoord2").setCustomValidity("wachtwoorden komen niet overeen");
	else
	document.getElementById("wachtwoord2").setCustomValidity(''); } 
	
	function validateEmail(){
	var em2=document.getElementById("email2").value;
	var em1=document.getElementById("email1").value;
	if(em1!=em2)
		document.getElementById("email2").setCustomValidity("E-mailadressen komen niet overeen");
	else
	document.getElementById("email2").setCustomValidity('');  
	}
</script>
<?php
	include "include/footer.php";
?>
</body>
</html>