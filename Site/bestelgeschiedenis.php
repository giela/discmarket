<?php 
include "include/header.php";
?>

<head>
	<!--Verwijderen als deze is toegevoegd in style.css-->
	<link rel="stylesheet" type="text/css" href="styles/styleadmin.css">
</head>

<div id="container">
	<div id="ordertabel">
<?php 
if(isset($CustomerID)){
$i =0;
$sql1="SELECT * FROM orders AS o JOIN orderdetail AS od ON od.OrderDetailID = o.OrderDetailID  Where CustomerID = $CustomerID";
$result = mysqli_query($GLOBALS['con'], $sql1);
$count = mysqli_num_rows($result);
	if ($count == 0)
	{
		echo "Geen bestellingen geplaatst";
	}
	else {
		echo "<table id='tabel1' border=1px class='table-hover'>";
			echo "<tr>";
			echo "<th> Bestelnummer:</th>";
			echo "<th> Besteldatum:</th>";
			echo "<th>Totaal aantal:</th>";
			echo "<th>Totaal prijs:</th>";
			echo "<th>Verzend Datum:</th>";
			echo "<th>BestelStatus:</th>";
			echo "<th>Bestellings informatie:";
			echo "<th>Extra info:</th>";
			echo "</tr>";
		
		while($row = mysqli_fetch_array($result)){
			$sql="SELECT p.ProductID, pa.Aantal, (pa.Aantal * p.ProductPrice) AS prijs FROM orders as o JOIN aantalprodperorder AS pa ON o.OrderID = pa.OrderID JOIN products AS p ON pa.ProductID = p.ProductID where CustomerID = $CustomerID and pa.OrderID = ".$row['OrderID']."";
			$result1=mysqli_query($GLOBALS['con'], $sql);

			while($row1 = mysqli_fetch_array($result1)){
				$i++;
				$totaalaantal[$i] = $row1['Aantal'];
				$prijs[$i] = $row1['prijs'];
			}
				$totaleaantal = array_sum($totaalaantal);
				$prijsaantal = count($prijs);
				$totaleprijs = array_sum($prijs);

			echo "<tr class='info'>";
			echo "<td>" . $row['OrderID'] . "</a></td>";
			echo "<td>" . $row['OrderDate'] . "</td>";
			echo "<td>" . $totaleaantal . " Artikelen</td>";
			echo "<td>&#8364;$totaleprijs</td>";
			echo "<td>";
			
			if($row['ShipDate'] == NULL) {
				echo "Niet verzonden";
			}
			else {
				echo $row['ShipDate'];
			}
			"</td>";
			echo "<td>" . $row['OrderDetail'] . "</td>";
			echo "<td><a href='CustomerOrderDetail.php?order_id=" .  $row['OrderID'] ."'>Besteldetails</a></td>";
			echo "<td>" ;
			if($row['OrderDetail'] == "Verzonden") {
				echo "Bezorgd door PostNL";
			}
			elseif($row['OrderDetail'] == "Geannuleerd"){
				echo"Geannuleerd";
			}
			else{
?>
			<a href='Annuleer.php?order_id="<?php echo $row['OrderID']; ?>"' onclick="return confirm('Weet u zeker dat u deze bestelling wilt annuleren?')">Annuleren</a>
<?php
			}
			echo "</td>";
			echo "</tr>";
			echo "<br>";
	

			unset($totaalaantal);
			unset($prijs);
		} //End while
		echo "</table>";
	}
}
else {
	header('Location: login');
}
?>
	</div><!--/ordertabel-->
</div><!--/container-->
<script type="text/javascript">
	jQuery(document).ready(function($) {
	    $(".clickable-row").click(function() {
	        window.document.location = $(this).data("href");
	    });
	});
</script>

<?php
include "include/footer.php";
?>
	</body>
</html>