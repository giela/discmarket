<?php //cart_update.php
session_start();
include_once("config.php");

//empty cart by distroying current session
if(isset($_GET["emptycart"]) && $_GET["emptycart"]==1)
{
	foreach($_SESSION["products"] as $reserveren){
		$product_code = $reserveren["ProductID"];
		$swag = $mysqli->query("UPDATE  cdtabel SET gereserveerd=NULL WHERE cdid='$product_code'");
	}
	$return_url = base64_decode($_GET["return_url"]); //return url 

	unset($_SESSION['products']);
	unset($_SESSION['verkoperid']);
	header('Location:'.$return_url);
}

//add item in shopping cart
if(isset($_POST["type"]) && $_POST["type"]=='add')
{
	$product_code 	= filter_var($_POST["product_code"], FILTER_SANITIZE_STRING); //product code
	$swag = $mysqli->query("UPDATE  cdtabel SET gereserveerd=1 WHERE cdid='$product_code'");

	
	$return_url 	= base64_decode($_POST["return_url"]); //return url
	//MySqli query - get details of item from db using product code
	$results = $mysqli->query("SELECT cdid,titel,prijs, gebruikerid FROM cdtabel WHERE cdid='$product_code' LIMIT 1");
	$obj = $results->fetch_object();
	
	if ($results) { //we have the product info 
		$_SESSION['verkoperid'] = $obj->gebruikerid;
		//prepare array for the session variable
		$new_product = array(array('ProductName'=>$obj->product_name, 'ProductID'=>$product_code, 'ProductPrice'=>$obj->ProductPrice));
		
		if(isset($_SESSION["products"])) //if we have the session
		{
			$found = false; //set found item to false
			
			foreach ($_SESSION["products"] as $cart_itm) //loop through session array
			{
				if($cart_itm["ProductID"] == $product_code){ //the item exist in array

					$product[] = array('ProductName'=>$cart_itm["ProductName"], 'ProductID'=>$cart_itm["ProductID"], 'ProductPrice'=>$cart_itm["ProductPrice"]);
					$found = true;
				}else{
					//item doesn't exist in the list, just retrive old info and prepare array for session var
					$product[] = array('ProductName'=>$cart_itm["ProductName"], 'ProductID'=>$cart_itm["ProductID"], 'ProductPrice'=>$cart_itm["ProductPrice"]);
				}
			}
			
			if($found == false) //we didn't find item in array
			{
				//add new user item in array
				$_SESSION["products"] = array_merge($product, $new_product);
			}else{
				//found user item in array list, and increased the quantity
				$_SESSION["products"] = $product;
			}
			
		}else{
			//create a new session var if does not exist
			$_SESSION["products"] = $new_product;
		}
		
	}
	
	//redirect back to original page
	header('Location:'.$return_url);
}

//remove item from shopping cart
if(isset($_GET["removep"]) && isset($_GET["return_url"]) && isset($_SESSION["products"]))
{
	$product_code 	= $_GET["removep"]; //get the product code to remove
	$swag = $mysqli->query("UPDATE  cdtabel SET gereserveerd=NULL WHERE cdid='$product_code'");
	$return_url 	= base64_decode($_GET["return_url"]); //get return url

	
	foreach ($_SESSION["products"] as $cart_itm) //loop through session array var
	{
		if($cart_itm["ProductID"]!=$product_code){ //item doesn't exist in the list
			$product[] = array('ProductName'=>$cart_itm["ProductName"], 'ProductID'=>$cart_itm["ProductID"], 'ProductPrice'=>$cart_itm["ProductPrice"]);
		}
		
		//create a new product list for cart
		$_SESSION["products"] = $product;
	}
	if(!isset($_SESSION['products'])){
		unset($_SESSION['verkoperid']);
	}
	//redirect back to original page
	header('Location:'.$return_url);
}
?>