<?php
include "include/header.php";
?>
<?php
requestid();
if(isset($CustomerID))
{
	admincheck($host, $username, $password, $db_name, $tbl_name, $CustomerID);
	$sql="SELECT UserName FROM $tbl_name WHERE CustomerID='$CustomerID'"; 
	$result=mysqli_query($GLOBALS["con"], $sql); 
	$row = mysqli_fetch_array($result);
}
else
{
	$rowadmin['Admin'] = 0;
}
?>

<head>
	<!--Verwijderen als deze is toegevoegd in style.css-->
	<link rel="stylesheet" type="text/css" href="styles/styleadmin.css">
</head>

<div id="container">
<?php
if(!isset($_GET['customer_id']))
{
	header('Location: crash.php');
}
else
{
	$klantid = $_GET['customer_id'];
}
admincheck($host, $username, $password, $db_name, $tbl_name, $CustomerID);	//returned $rowadmin zodat je kan checken of iemand wel een admin is.
if($rowadmin['Admin'] == 0)
{
	if(isset($CustomerID))
	{
		echo "U heeft geen toegang tot deze pagina! ";
		echo "<a href='home.php'>home</a>";
	}
	else
	{
		echo "U heeft geen toegang tot deze pagina! ";
		echo "<a href='inlog.php'>login</a>";
	}
}
else{
?>
<aside>
	<a href="Productentoevoegen.php"><div id="pt" class="buttons">Producten toevoegen</div></a>
	<a href="productkiezen.php"><div id="pk" class="buttons">Producten wijzigen</div></a>
	<a href="Productaanvullen"><div id="pk" class="buttons">Product aanvullen</div></a>
	<a href="productkiezenverwijder.php"><div id="pv" class="buttons">Producten verwijderen</div></a>
	<a href="adminpanel.php"><div id="pt" class="buttons">Order status beheer</div></a>
	<a href="accountbeheer.php"><div id="pt" class="buttons">Account beheer</div></a>
</aside>
<div id="admincontainer">
	<div id="titel">
		Klant overzicht van klantnr: <?php echo $klantid; ?>
	</div>
	<div id="filter">
	</div>
	<form id='statuswijzig' method='post' name='statuswijzig' action='verwijderklant.php'>
	<div id="orders">
<?php 
echo "
	<table>
		<tr>
		<th>Klantnr</th>
		<th>Naam</th>
		<th>Postcode</th>
		<th>adress</th>
		<th>Stad</th>
		<th>Telefoon & mobiel</th>
		</tr>";
$sql ="SELECT DISTINCT c.CustomerID, c.Firstname, c.Lastname, c.Insertion, c.Postalcode, c.Street, c.Housenumber, c.extension, c.City, c.PhoneNumber, c.MobileNumber, c.EmailAdress, c.LastOnline, c.Datecreated FROM customers c WHERE CustomerID= ' ". $klantid." ' " ;
$result = mysqli_query($GLOBALS['con'], $sql);
$sql1 ="SELECT COUNT(o.OrderID) as bestellingen FROM customers c JOIN orders o ON o.CustomerID=c.CustomerID WHERE c.CustomerID= ' ". $klantid." ' " ;
$result1 = mysqli_query($GLOBALS['con'], $sql1);
$row1 = mysqli_fetch_array($result1);
$row = mysqli_fetch_array($result);
	echo "<tr>";
	echo "<td>".  $row['CustomerID'] . "</td>";
	echo "<td>" . $row['Firstname']." ". $row['Insertion']." ".$row['Lastname']  . "</td>";
	echo "<td>" . $row['Postalcode'] . "</td>";
	echo "<td>" . $row['Street']." ".$row['Housenumber']  . "</td>";
	echo "<td>" .  $row['City'] . "</td>";
	echo "<td>" . $row['PhoneNumber'] . " <br> " . $row['MobileNumber'] . "</td>";
	echo "</tr></table>";
	echo "<table>
			<tr>
				<th>Bestellingen</th>
				<th>Aangemaakt</th>
				<th>Laatst ingelogd</th>
			</tr>";		
	echo "<tr>";
	echo "<td>". $row1['bestellingen']. "</td>";
	echo "<td>". $row['Datecreated']. "</td>";
	echo "<td>". $row['LastOnline']. "</td>";
	echo "</tr>";
echo "</table>";
?>
	</div><!--/orders-->
	<div id="minifooter"> 
		<input type="hidden" name="klantid" value="<?php echo $klantid; ?>">	
		<input type="submit" name="submit" value="verwijder" onclick="return confirm('Are you sure?');"/>
	</div>
	</form>
	</div><!--/admincontainer-->
</div><!--/container-->
<?php 
} //End Else
mysqli_close($GLOBALS['con']);
?>
<?php
include "include/footer.php";
?>
</body>
</html>