<?php //Product details
include_once("config.php");
$db_name="discmarket"; // Database name 

$art_id = $_GET['art_id'];

$default = "mm";
$size = 40;

include "include/header.php";
requestid();
$current_url = base64_encode($url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);

$results = $mysqli->query("SELECT cdtabel.cdid, cdtabel.titel, cdtabel.kwaliteit, cdtabel.prijs, cdtabel.beschrijving, cdtabel.afbeelding, cdtabel.gereserveerd, gebruiker.gebruikerid, gebruiker.naam, gebruiker.woonplaats, uitgever.uitgever, genre.genre, artiest.artiest, cdtabel.artiestid, gebruiker.email 
FROM cdtabel 
JOIN gebruiker ON cdtabel.gebruikerid=gebruiker.gebruikerid
JOIN uitgever ON uitgever.uitgeverid=cdtabel.uitgeverid 
JOIN artiest ON artiest.artiestid=cdtabel.artiestid 
JOIN genre ON genre.genreid=cdtabel.genreid
AND cdtabel.cdid='". $art_id ."'");
?>


<?php
if ($results) {
	while($obj = $results->fetch_object()) {
	$email = $obj->email;
	$grav_url = "http://www.gravatar.com/avatar/" . md5( strtolower( trim( $email ) ) ) . "?d=" . urlencode( $default ) . "&s=" . $size;
echo'
<div class="container">
	<label>' . $obj->artiest . " - " . $obj->titel . '</label>	
	<div class="row">
		<div class="col-md-5">
			<img src="img/' . ($obj->afbeelding == null ? "noimg.gif" : $obj->afbeelding) . '" id="plaatje"/>
		</div>	 
		<div class="col-md-4">
			<div class="titel">
				<label>Titel:</label> ' . $obj->titel . '
			</div>
			<div class="artiest">
				<label>Artiest:</label> <a href="productvangebruiker?artiestid='. $obj->artiestid.'">'.$obj->artiest.'</a>
			</div>
			<div class="uitgever">
				<label>Uitgever:</label> ' . $obj->uitgever . '
			</div>
			<div class="genre">
				<label>Genre:</label> ' . $obj->genre . '
			</div>
			<div class="kwaliteit">
				<label>Kwaliteit:</label> <meter value="' . $obj->kwaliteit . '" min="0" max="5"></meter>
			</div>
		</div>	
		<div class="col-md-3" >
		<label><h4>&euro;' . $obj->prijs . '</h4></label>
			<div id="borderbottom">
				

				<form method="post" action="cart_update.php">
				<div class="form-group">';
				if($CustomerID != $obj->gebruikerid){
					if($obj->gereserveerd == null){
				echo '<button type="buton" class="btn" id="winkelwagenknop">In winkelwagen</button>';
					}
				}
				echo '<input type="hidden" name="product_code" value="'.$art_id.'" />';
				echo '<input type="hidden" name="type" value="add" />';
				echo '<input type="hidden" name="return_url" value="'.$current_url.'" />';
				echo '</form>';
				if($CustomerID == $obj->gebruikerid){
				echo '<form method="post" action="productenwijzigen.php">
				<input type="hidden" name="productgekozen" value="'.$art_id.'"/>
				<p><button type="buton" class="btn">Wijzig advertentie</button></p>
				</form>';
				echo '<form method="post" action="productverwijder.php">
				<input type="hidden" name="productgekozen" value="'.$art_id.'"/>'; ?>
				<button type="buton" class="btn" onclick="return confirm('Weet u het zeker?');"> Verwijder deze advertentie! </button> 

				<?php
				echo '</form>';
				}
				echo'
						</div><!--/Right-->
				';

				echo'
				<label class="conditie">Conditie</label>
			</div>

			<div class="hoesje">
				<label>Hoesje:</label> Nieuw
			</div>
			<div class="cd">
				<label>CD-ROM:</label> Nieuw
			</div>
			<div id="borderbottom">
				<div class="verkoper">
					<label>Verkoper:&nbsp</label><a href="productvangebruiker?uid=' . $obj->gebruikerid . '">' . $obj->naam . ' <img class="circular" src='.$grav_url.' alt="" />
					</a>
				</div>
			</div>
			<div class="versturen">
				<label>Verstuurd vanaf:</label> ' . $obj->woonplaats . '
			</div>
			<div class="informatie">
				<label>Payment Information:</label> PayPal, iDeal
			</div>
		</div>
	</div>		
				

	<div class="row">
		<div class="col-md-12" >
			<div class="bordertop">
				<div class="beschrijving">
				<label>Beschrijving:</label><p>' . $obj->beschrijving . '</p>
				</div>
			</div>
		</div>
	</div>
</div><!--/container -->
';
	}
}
?>
<?php
include "include/footer.php";
?>
</body>
</html>