<!DOCTYPE html>
<html>
<body>
<?php
$q = intval($_GET['q']);

require "functions.php";
include_once("config.php");
dbconnect($host, $username, $password, $db_name);

if($q == 0)
{
	$sql="SELECT c.CustomerID, c.UserName, c.Datecreated, c.LastOnline FROM customers c ORDER BY Datecreated ASC"; 
}
else
{
	$sql="SELECT c.CustomerID, c.UserName, c.Datecreated, c.LastOnline FROM customers c ORDER BY Datecreated DESC"; 
}
$result = mysqli_query($GLOBALS['con'], $sql);
echo "
<table>
	<tr>
	<th>CustomerID</th>
	<th></th>
	<th>Gebruikers naam</th>
	<th>Aangemaakt op</th>
	<th>Laatst ingelogd</th>
	</tr>";
while($row = mysqli_fetch_array($result))
{
    echo "<tr>";
	echo "<td><a href='orderdetail.php?order_id=" .$row['CustomerID'] ."'>" . $row['CustomerID'] . "</a></td>";
	echo "<td id='check'><input type='checkbox' name='check_list[".$row['CustomerID']."]' value=".$row['CustomerID']."></td>";
    echo "<td>" . $row['UserName']."</td>";
    echo "<td>" . $row['Datecreated'] . "</td>";
	echo "<td>" . $row['LastOnline'] . "</td>";
    echo "</tr>";
}
echo "</table>";
mysqli_close($GLOBALS['con']);
?>
</body>
</html>