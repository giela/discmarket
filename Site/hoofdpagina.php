<?php
include "include/header.php";
include_once("config.php");
include_once("include/paginate.php");
error_reporting(-1);
ini_set('display_errors', 'On');
if(isset($_SESSION['verkoperid'])){
	$verkoperid = $_SESSION['verkoperid']; 
}
else{
	unset($_SESSION['products']);
}
//current URL of the Page. cart_update.php redirects back to this URL
$current_url = base64_encode($url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
//echo $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
$item_per_page = 12; //item to display per page

if(isset($_GET["page"])){ //Get page number from $_GET["page"]
  $page_number = filter_var($_GET["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
  if(!is_numeric($page_number) || $page_number < 1){/*die('Invalid page number!');*/ $page_number = 1;} //incase of invalid page number
}else{
  $page_number = 1; //if there's no page number, set it to 1
}
if(isset($_GET['genreid'])){
	$gID = $_GET['genreid'];
	if(!isset($_SESSION['verkoperid'])){
	$total = $mysqli->query("SELECT COUNT( cdid ) FROM cdtabel WHERE genreid= $gID ");
}
	else{
		$total = $mysqli->query("SELECT COUNT( cdid ) FROM cdtabel WHERE genreid= $gID AND gebruikerid = $verkoperid ");
	}
} 
else{
	if(isset($_SESSION['verkoperid'])){
	$total = $mysqli->query("SELECT COUNT( cdid ) FROM cdtabel WHERE gebruikerid =$verkoperid");
	}
	else{
$total = $mysqli->query("SELECT COUNT( cdid ) FROM cdtabel");
	}
}
//$total = $mysqli->query("SELECT COUNT( cdid ) FROM cdtabel JOIN genre ON cdtabel.genreid = genre.genreid AND genre.genreid =1");
$get_total_rows = $total->fetch_row(); //hold total records in variable
$total_pages = ceil($get_total_rows[0]/$item_per_page); //break records into pages

$page_position = (($page_number-1) * $item_per_page); //get starting position to fetch the records
?>

<div class="col-md-2">
    <ul class="nav nav-pills nav-stacked">
    <p><li class='genre'><b>Genre</b></li></p>
    <li><a href="hoofdpagina.php">Alle</a></li>
      <?php  
        $allGenres = $mysqli->query("SELECT DISTINCT cdtabel.genreid, genre.genre FROM cdtabel JOIN genre ON genre.genreid=cdtabel.genreid");
        while($row = mysqli_fetch_array($allGenres)) {
        echo "<li><a href='hoofdpagina.php?genreid=".$row['genreid']." '>". $row['genre'] ."</a></li>";
        } 
      ?>
    </ul>
</div>

<div class="container col-md-10">

  <div class="row">
<?php
  if(isset($_GET['genreid']))
  {
	  if(isset($_SESSION['verkoperid'])){
			$results = $mysqli->query("SELECT cdtabel.cdid, cdtabel.titel, cdtabel.beschrijving, cdtabel.gereserveerd, cdtabel.prijs, cdtabel.afbeelding, cdtabel.kwaliteit, uitgever.uitgever, genre.genre, artiest.artiest FROM cdtabel 
		  JOIN uitgever ON uitgever.uitgeverid=cdtabel.uitgeverid 
		  JOIN artiest ON artiest.artiestid=cdtabel.artiestid      
		  JOIN genre ON genre.genreid=cdtabel.genreid
		  WHERE genre.genreid= $gID AND cdtabel.gebruikerid = $verkoperid ORDER BY uploaddatum DESC LIMIT $page_position, $item_per_page");   
	   }
	   else{
		  $results = $mysqli->query("SELECT cdtabel.cdid, cdtabel.titel, cdtabel.beschrijving, cdtabel.prijs, cdtabel.afbeelding, cdtabel.gereserveerd, cdtabel.kwaliteit, uitgever.uitgever, genre.genre, artiest.artiest FROM cdtabel 
		  JOIN uitgever ON uitgever.uitgeverid=cdtabel.uitgeverid 
		  JOIN artiest ON artiest.artiestid=cdtabel.artiestid      
		  JOIN genre ON genre.genreid=cdtabel.genreid
		  WHERE genre.genreid= $gID ORDER BY uploaddatum DESC LIMIT $page_position, $item_per_page");
	   }
  } 
    else
  {
	  if(isset($_SESSION['verkoperid'])){
	  $results = $mysqli->query("SELECT cdtabel.cdid, cdtabel.titel, cdtabel.beschrijving, cdtabel.prijs, cdtabel.afbeelding, cdtabel.gereserveerd, cdtabel.kwaliteit, uitgever.uitgever, genre.genre, artiest.artiest FROM cdtabel 
	  JOIN uitgever ON uitgever.uitgeverid=cdtabel.uitgeverid 
	  JOIN artiest ON artiest.artiestid=cdtabel.artiestid
	  JOIN genre ON genre.genreid=cdtabel.genreid
	  WHERE cdtabel.gebruikerid = $verkoperid
	  ORDER BY uploaddatum DESC LIMIT $page_position, $item_per_page");
	  }
	  else{  
      $results = $mysqli->query("SELECT cdtabel.cdid, cdtabel.titel, cdtabel.beschrijving, cdtabel.prijs, cdtabel.afbeelding, cdtabel.kwaliteit, cdtabel.gereserveerd, uitgever.uitgever, genre.genre, artiest.artiest FROM cdtabel 
      JOIN uitgever ON uitgever.uitgeverid=cdtabel.uitgeverid 
      JOIN artiest ON artiest.artiestid=cdtabel.artiestid
      JOIN genre ON genre.genreid=cdtabel.genreid
      ORDER BY uploaddatum DESC LIMIT $page_position, $item_per_page");
	  }
  }
?>
<?php 
  if($results){
    while($obj = $results->fetch_object()){
//echo '<form method="post" action="cart_update.php">';
echo'<a href="detail.php?art_id=' .  $obj->cdid . '" title="'.$obj->artiest." " . $obj->titel.'">';?>
      <div id="test" class="row col-xs-12 col-sm-6 col-md-2">
        <div class="thumbnail" >
          <?php echo'<img src="img/'.($obj->afbeelding == null ? "noimg.gif" : $obj->afbeelding).'" class="productpage_img">'; ?>
          <div class="caption">
            <div class="row">
              <div class="col-md-6 col-xs-6 price">
                <h3><label><?php echo $currency.$obj->prijs; ?></label></h3>
              </div>
			  <div class="col-md-6 col-xs-3 stock">
                <h6><?php if($obj->gereserveerd != null){ echo "gereserveerd";}?></h6>
              </div>
            </div>  
            <div class="row">
              <div class="col-md-12" style=" white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
              <b><?php echo $obj->titel; ?></b>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12" style=" white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
              <?php echo $obj->artiest; ?>
              </div>
            </div>
          </div>
        </div>
      </a>
    </div>
<?php
    //  echo '<input type="hidden" name="product_code" value="'.$obj->cdid.'" />';
     // echo '<input type="hidden" name="type" value="add" />';
     // echo '<input type="hidden" name="return_url" value="'.$current_url.'" />';
     // echo '</form>';
    }
  }
?>
  </div>
<?php
if(!isset($gID)){
$gID = NULL;
}
echo paginate($item_per_page, $page_number, $total_pages, $gID);

?>
</div>
<?php 
include "include/footer.php";
?>

</body>
</html>