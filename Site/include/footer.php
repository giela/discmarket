<head>
	<link rel="stylesheet" type="text/css" href="styles/footer.css">
</head>
<body>

<div class="outer_footer navbar-fixed-bottom">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="footer">
					<h5>&copy; Copyright 2015 Discmarket</h5>
				</div>
			</div>
		</div>
	</div>
</div>
</body>
</html>