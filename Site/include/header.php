<!DOCTYPE html>
<?php
error_reporting(0);
include_once("config.php");
$tbl_name="gebruiker"; // Table name  
//$ipadres = $_SERVER['REMOTE_ADDR'];
require "functions.php";
dbconnect($host, $username, $password ,$db_name);
$total = 0;
session_start();
if(isset($_SESSION["products"])){
	foreach ($_SESSION["products"] as $cart_itm)
			{
				$total ++;
			}			
}
else{
	$total=" ";
}
//$default = "mm";
//$size = 40;

/*
$sqlip="SELECT ipadres FROM ipadress"; 
$resultip=mysqli_query($GLOBALS["con"], $sqlip); 
$ips = mysqli_fetch_array($resultip);
if(!in_array($ipadres, $ips))
{
$sqlip1="INSERT INTO ipadress (ipadres) VALUES('$ipadres')";
mysqli_query($GLOBALS["con"], $sqlip1); 
}
*/
requestid();
if(isset($CustomerID)){
	admincheck($host, $username, $password, $db_name, $tbl_name, $CustomerID);
	$sql="SELECT naam, email FROM $tbl_name WHERE gebruikerid='$CustomerID'"; 
	$result=mysqli_query($GLOBALS["con"], $sql); 
	$row = mysqli_fetch_array($result);
}
else
{
	$rowadmin['admin'] = 0;
}
?>

<html lang="nl">
<head>
<title>Discmarket</title>
<link rel="icon" href="img/media_vinyl_79.ico" type="image/x-icon" />
	
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-62249130-1', 'auto');
ga('send', 'pageview');
</script>

<!-- Bootstrap compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
<!-- AJAX -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<!-- Normal CSS file -->
<link rel="stylesheet" type="text/css" href="styles/style.css">
<!--productpagina extra-->
<script src='https://www.google.com/recaptcha/api.js'></script><!--Captcha-->

</head>
<body>

  
<!--onder header-->
<nav class="navbar navbar-default header-color">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="hoofdpagina">discmarket.nl</a>
    </div>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
<?php
    if(!isset($CustomerID))
    {
?>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="inlogpagina">Inloggen</a></li>
      </ul>
<?php
    }
    else
    {
		//$email = $row['email'];
		//$grav_url = "http://www.gravatar.com/avatar/" . md5( strtolower( trim( $email ) ) ) . "?d=" . urlencode( $default ) . "&s=" . $size;
      echo'
            <ul class="nav navbar-nav navbar-right">
              <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">'. $row['naam'] .' <span class="caret"></span></a>
                <ul class="dropdown-menu" role="menu">
            <li><a href="Productentoevoegenverkoper">Verkoop</a></li>
            <li><a href="productvangebruiker?uid='. $CustomerID.' ">Mijn advertenties</a></li>
            <li class="divider"></li>
                  <li><a href="persoonlijkewijzigform">Gegevens Wijzigen</a></li>
                  <li><a href="accountgegevenswijzigform">Account Wijzigen</a></li>
                  <li class="divider"></li>
                  <li><a href="logout">Uitloggen</a></li>
            ';
            if($rowadmin['admin'] >= 1 ){
            echo'<li><a href="adminpanel">Admin Panel</a></li>';
            }
                 echo '<li class="divider"></li>
                </ul>
            </li>';     
      echo '</ul>';          
    }
?>
    <form class="navbar-form navbar-right" action="zoekresultaten.php" role="search" method="post">
      <div class="input-group">
        <input type="text" class="form-control" name="search" placeholder="Op zoek naar...">
        <span class="input-group-btn">
          <button class="btn btn-default" type="submit">Zoeken</button>
        </span>
      </div><!-- /input-group -->
    </form>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

<!--fixed header-->
<nav class="navbar-fixed-top navbar-default header-color">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="hoofdpagina">discmarket.nl</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
<?php
    if(!isset($CustomerID))
    {
?>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="inlogpagina">Inloggen</a></li>
      </ul>
<?php
    }
		else
		{
			echo'
        		<ul class="nav navbar-nav navbar-right">
    	        <li class="dropdown">
          		<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">'. $row['naam'] .' <span class="caret"></span></a>
          			<ul class="dropdown-menu" role="menu">
						<li><a href="Productentoevoegenverkoper">Verkoop</a></li>
						<li><a href="productvangebruiker?uid='. $CustomerID.' ">Mijn advertenties</a></li>
						<li class="divider"></li>
			            <li><a href="persoonlijkewijzigform">Gegevens Wijzigen</a></li>
			            <li><a href="accountgegevenswijzigform">Account Wijzigen</a></li>
			            <li class="divider"></li>
			            <li><a href="logout">Uitloggen</a></li>
						';
						if($rowadmin['admin'] >= 1 ){
						echo'<li><a href="adminpanel">Admin Panel</a></li>';
						}
			           echo '<li class="divider"></li>
          			</ul>
        		</li>';			
      echo '</ul>';          
    }
?>
    <form class="navbar-form navbar-right" action="zoekresultaten.php" role="search" method="post">
      <div class="input-group">
        <input type="text" class="form-control" name="search" placeholder="Op zoek naar...">
        <span class="input-group-btn">
          <button class="btn btn-default" type="submit">Zoeken</button>
        </span>
      </div><!-- /input-group -->
    </form>

	 <form class="navbar-form navbar-right" action="winkelwagen.php" >
      <div class="input-group">
        <span class="input-group-btn">
          <button class="btn btn-default" type="submit">Winkelwagen<?php echo " ". $total;?></button>
        </span>
      </div><!-- /input-group -->
    </form>
	
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
<!--#-->
<div class="x"></div>
