<?php 
include "include/header.php";
?>
<div id="containerinloggen" class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-bottom: 404px;"> 
	<div id="inlogscherm" class="col-md-offset-2 col-lg-offset-3  col-xs-12 col-sm-12 col-md-8 col-lg-6">
	<form name="form1" method="POST" action="login.php" class="form-horizontal"> 
		<div id="links" class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
			<div class="form-group">
				<div class="col-md-offset-2 col-md-2">
					<label id="inloggen">Inloggen</label>
				</div>
			</div>		
			<div class="form-group">
				<div class="col-xs-3 col-sm-3 col-md-5 col-lg-5 control-label">Email</div>
				<div class="col-sm-7">
					<input type="text" name="myusername" class="form-control" id="myusername" placeholder="Email">
				</div>
			</div>
			<div class="form-group">
			<p>
				<div class="col-xs-3 col-sm-3 col-md-5 col-lg-5 control-label">Wachtwoord</div>
				<div class="col-sm-7">
					<input type="password" name="mypassword" class="form-control" id="mypassword" placeholder="Wachtwoord">
				</div>
			</p>
			</div>
			<div class="form-group">
				<p>
					<div class="col-md-offset-5 col-sm-3 col-md-5 col-lg-5">
						<button type="submit" class="btn btn-default">Sign in</button>
					</div>
			</p>
			</div>
			<div class="form-group">
				<div class="col-md-offset-5 col-sm-12 col-md-12 col-lg-12">
					<div class="checkbox">
						
						<p>	<label><input type="checkbox" name="checkbox" value="YES"> Blijf ingelogd</label> </p>
					</div>
				</div>
			</div>
			<div class="form-group">	
				<div class="col-md-offset-5 col-sm-12 col-md-12 col-lg-12">	
<?php 
				if (isset($_SESSION['Tries']))
				{
					$tries = $_SESSION['Tries'];
					echo $tries;
					echo "<td><td> keer verkeerd</td> <td>ingelogd</td></td><br>";
					if ($tries >= 5)
					{
						echo "<br><div class='g-recaptcha' data-sitekey='6LfiUgMTAAAAAICOONiYkCYCNh6mtCCIumH4SUWz'></div>";
					}
				}
?>
				</div>
			</div>
		</div>
		<input type="hidden" name="pagina" value="<?php echo $_SERVER['HTTP_REFERER']; ?>">
	</form><!--/forminloggen-->
		<div id="registreren" class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
			<form class="form-horizontal"> 
				<div class="form-group">
					<div class="col-md-offset-2 col-md-2">
						<label>Registreren</label>
					</div>
				</div>
				<div class="form-group">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 control-label" id="profiel">	
						Nog geen bestaande klant?
						<p>Maak snel een profiel aan.</p>	
					</div>
				</div>
				<div class="form-group">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 control-label">	
						<a href="Registratie.php" class="btn btn-default" role="button">Registreren</a>
					</div>
				</div>
			</form>
		</div><!--/registreren-->
	</div><!--/inlogscherm-->
</div><!--/containerinloggen-->
<?php 
include "include/footer.php";
?>
</body>
</html>