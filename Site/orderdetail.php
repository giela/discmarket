<?php 
	include "include/header.php";
?>
<?php
//does get order_id actually contain something if not go to crash php
if(!isset($_GET['order_id']))
{
	header('Location: crash.php');
}
else
{
	$OrderID = $_GET['order_id'];
}
admincheck($host, $username, $password, $db_name, $tbl_name, $CustomerID);	//returned $rowadmin zodat je kan checken of iemand wel een admin is.
if($rowadmin['Admin'] == 0)
{
	if(isset($CustomerID))
	{
		echo "U heeft geen toegang tot deze pagina! ";
		echo "<a href='home.php'>home</a>";
	}
	else
	{
		echo "U heeft geen toegang tot deze pagina! ";
		echo "<a href='inlog.php'>login</a>";
	}
}
else
{ 
$sql="SELECT DISTINCT OrderDetailID, OrderDetail FROM orderdetail";
$result=mysqli_query($GLOBALS["con"], $sql)  or die(mysqli_error($db_name));
}
//$sql="OrderID, c.Lastname, OrderDate FROM  ";
?>
<html>
<head>
	<title>Admin panel</title>
	<link rel="stylesheet" type="text/css" href="styles/styleadmin.css">
	<script>
		function showOrders(str) 
		{
			if (str == "")
			{
				document.getElementById("orders").innerHTML = "";
				return;
			} 
			else 
			{ 
				if (window.XMLHttpRequest) 
				{
					// code for IE7+, Firefox, Chrome, Opera, Safari
					xmlhttp = new XMLHttpRequest();
				} 
				else 
				{
					// code for IE6, IE5
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				}
				xmlhttp.onreadystatechange = function() 
				{
					if (xmlhttp.readyState == 4 && xmlhttp.status == 200) 
					{
						document.getElementById("orders").innerHTML = xmlhttp.responseText;
					}
				}
				xmlhttp.open("GET","getorder.php?q="+str,true);
				xmlhttp.send();
			}
		}
	</script>
</head>
<body>
    <div id="container">
		<div id="contentadminpanel">	
      		<aside>
	    		<a href="Productentoevoegen.php"><div id="pt" class="buttons">Producten toevoegen</div></a>
	    		<a href="productkiezen.php"><div id="pk" class="buttons">Producten wijzigen</div></a>
	    		<a href="Productaanvullen"><div id="pk" class="buttons">Product aanvullen</div></a>
	    		<a href="productkiezenverwijder.php"><div id="pv" class="buttons">Producten verwijderen</div></a>
	    		<a href="adminpanel.php"><div id="pt" class="buttons">Order status beheer</div></a>
	    		<a href="accountbeheer.php"><div id="pt" class="buttons">Account beheer</div></a>
      		</aside>
      		
      		<div id="admincontainer">
	      		<div id="titel">
				Order overzicht van ordernr: <?php echo $OrderID; ?>
	    		</div>
	    		
	    		<div id="filter">
				<form id="form">
		  		Filter op status:
					<select name="Orderstatus" id="orderstatusselect" onchange="showOrders(this.value)">
						<option value="0" selected>Alles</option>
						<?php foreach($result as $value)
						{
						?>
						<option value="<?php echo $value['OrderDetailID'];?>">
							<?php echo $value['OrderDetail'];?>
						</option>
						<?php 
						} 
						?>
					</select>
					<label id="aantalselect">
					Aantal per pagina:
					</label>
					<select name="aantal">
			    		<option value="10">10</option>
			    		<option value="20">20</option>
			    		<option value="30">30</option>
			    	</select>
		  		</form>
	    		</div>

				<form id='statuswijzig' method='post' name='statuswijzig' action='statuschange.php'>
					<div id="orders">
					<?php 
					echo "<table>
							<tr>
							<th>CustomerID</th>
							<th>Naam</th>
							<th>Postcode</th>
							<th>adress</th>
							<th>Stad</th>
							</tr>";
						$sql ="SELECT DISTINCT c.CustomerID, c.Firstname, c.Lastname, c.Insertion, c.Postalcode, c.Street, c.Housenumber, c.extension, c.City FROM orders o JOIN customers c ON c.CustomerID=o.CustomerID WHERE OrderID= ' ". $OrderID." ' " ;
						$result1 = mysqli_query($GLOBALS['con'], $sql);
						while($row = mysqli_fetch_array($result1))
						{
							echo "<tr>";
							echo "<td>".  $row['CustomerID'] . "</td>";
							echo "<td>" . $row['Firstname']." ". $row['Insertion']." ".$row['Lastname']  . "</td>";
							echo "<td>" . $row['Postalcode'] . "</td>";
							echo "<td>" . $row['Street']." ".$row['Housenumber']  . "</td>";
							echo "<td>".  $row['City'] . "</td>";
							echo "</tr>";
						}
						$sql1 ="SELECT DISTINCT p.ProductName, p.ProductID, ap.Aantal  FROM aantalprodperorder ap JOIN products p ON p.ProductID=ap.ProductID WHERE ap.OrderID= ' ". $OrderID." ' " ;
						$result2 = mysqli_query($GLOBALS['con'], $sql1);
								echo "<table>
									<tr>
									<th>ProductID</th>
									<th>Product naam</th>
									<th>aantal</th>
									</tr>";
						while($row2 = mysqli_fetch_array($result2))
						{
							echo "<tr>";
							echo "<td>".  $row2['ProductID'] . "</td>";
							echo "<td>" . $row2['ProductName']. "</td>";
							echo "<td>" . $row2['Aantal'] . "</td>";
							echo "</tr>";
						}
							echo "</table>";
					?>
					</div>

					<div id="minifooter"> 	
						<select name="Orderstatuswijzig" id="Orderstatuswijzig">
							<?php foreach($result as $value){?><option value="<?php echo $value['OrderDetailID'];?>"><?php echo $value['OrderDetail'];?></option><?php } ?></select>
						</select>
						<input type="submit" name="submit" value="wijzig status"/>	
					</div>

				</form>
			</div> <!--admincontainer-->
		</div> <!--contentadminpanel-->
	</div> <!--container-->
<?php
	include "include/footer.php";
?>
</body>
</html>
<?php } mysqli_close($GLOBALS['con']);?>