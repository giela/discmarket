<?php
  include "include/header.php";
?>
<?PHP
  requestid();
  if(isset($CustomerID))
  {
    $sqlcust="SELECT * FROM gebruiker where gebruikerid ='$CustomerID'";
    $resultcust=mysqli_query($GLOBALS["con"], $sqlcust)  or die(mysqli_error($db_name));
    $rowcust= $resultcust->fetch_array(MYSQLI_ASSOC); 
  }
  else 
  {
    header("Location:inlogpagina.php");
  }
?>
<html>
  <head>
   <title>Persoonlijke Gegevens wijzigen</title>
   <link rel="stylesheet" type="text/css" href="styles/stylegegevenswijzigen.css">
  </head>
<body>
  <div class="containergegevens">
    <form class="form-horizontal" action="persoonlijkegegevenswijzigen.php" method="post">
      <div class="form-group">
        <label class="col-sm-offset-2 col-sm-2">Gegevens wijzigen</label>
      </div>
      <div class="form-group">
        <label for="inputName3" class="col-sm-2 control-label">Naam</label>
          <div class="col-sm-2">
            <input type="text" class="form-control" placeholder="naam" name="naam" value="<?php echo $rowcust['naam']; ?>">
          </div>
      </div>

      <div class="form-group">
        <label for="inputPostalCode3" class="col-sm-2 control-label">Postcode</label>
        <div class="col-sm-2">
          <input type="text" class="form-control" placeholder="1234AB" name="postcode" pattern="[0-9]{4}[A-Z]{2}" value="<?php echo $rowcust['postcode']; ?>">
        </div>
      </div>

      <div class="form-group">
        <label for="inputPostalCode3" class="col-sm-2 control-label">Woonplaats</label>
        <div class="col-sm-2">
          <input type="text" class="form-control" name="woonplaats" value="<?php echo $rowcust['woonplaats']; ?>">
        </div>
      </div>


      <div class="form-group">  
        <div class="col-sm-offset-2 col-sm-2">
          <button type="submit" class="btn btn-default">Wijzig</button>
        </div>
      </div>  
    </form>
  </div>
<?php
include "include/footer.php";
?>  
</body>
</html>