<?php 
include "include/header.php";
?>
<html>
	<head>
		<title>Webshop infologic</title>
		<link rel="stylesheet" type="text/css" href="styles/style.css">
		<link rel="stylesheet" type="text/css" href="styles/styleadmin.css">
	</head>
	<body>				
		<div id="container">
			<?PHP
					if(!isset($CustomerID))
					{
						header("location:inlogpagina");
					}
					else
					{
						$sqlprod="SELECT titel, artiest, cdid FROM cdtabel join artiest on cdtabel.artiestid=artiest.artiestid where gebruikerid=$CustomerID";
						$resultprod=mysqli_query($GLOBALS["con"], $sqlprod)  or die(mysqli_error($GLOBALS["con"]));
			?>
				<div id="container2">
					<div id="contentadminpanel">
						<aside>
							<a href="Productentoevoegenverkoper"><div id="pt" class="buttons">Verkopen</div></a>
							<a href="productkiezen"><div id="pk" class="buttons">Producten wijzigen</div></a>
							<a href="productkiezenverwijder"><div id="pv" class="buttons">Producten verwijderen</div></a>
						</aside>
						<div id="admincontainer">
							<div id="titel">
								Producten wijzigen
							</div><!--titel-->
							<div id="filter">
								<h3>kies het product dat je wilt wijzigen</h3>
							</div><!--filter-->
							<div id="orders">
								<form id="kiesform" name="productkiezen" method="POST" action="productenwijzigen.php" >
									Product <select name= "productgekozen" id="productselect" onchange="this.form.submit()" required>
									<option value="NULL" selected>Kies een product</option>
									<?php foreach($resultprod as $value){?><option value="<?php echo $value['cdid'];?>"><?php echo $value['titel']." ".$value['artiest'];?></option><?php } ?></select>
								</form>		   
							</div><!--orders-->
							<div id="minifooter">
							
							</div><!--minifooter-->
						</div><!-- admincontainer-->
					<?php } ?>
					</div><!--contentadminpanel-->
				</div><!--Container2-->
		</div><!--Container -->
		<?php
			include "include/footer.php";
		?>
	</body>
</html>