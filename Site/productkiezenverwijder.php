<?php 
include "include/header.php";
?>
<html>
<head>
	<title>Webshop infologic</title>
	<link rel="stylesheet" type="text/css" href="styles/style.css">
	<link rel="stylesheet" type="text/css" href="styles/styleadmin.css">
</head>
<body>
	
<div id="container">
<div id="contentadminpanel">
<?PHP
if($rowadmin['admin'] == 0)
{
	if(isset($CustomerID))
	{
		$sqlprod="SELECT titel, cdid FROM cdtabel where gebruikerid = $CustomerID";
		$resultprod=mysqli_query($GLOBALS["con"], $sqlprod)  or die(mysqli_error($GLOBALS["con"]));
		?>
		<aside>
			<a href="Productentoevoegenverkoper"><div id="pt" class="buttons">Verkopen</div></a>
			<a href="productkiezen"><div id="pk" class="buttons">Producten wijzigen</div></a>
			<a href="productkiezenverwijder"><div id="pv" class="buttons">Producten verwijderen</div></a>
		</aside>
		<div id="admincontainer">
			<div id="titel">
				Producten verwijderen
			</div>
			<div id="filter">
				<h4>kies het product dat je wilt verwijderen</h4>
			</div>
			<div id="orders">
				<form id="kiesform" name="productkiezen" method="POST" action="productverwijder.php" >
				Product: <select name= "productgekozen" id="productselect"required>
				<?php foreach($resultprod as $value){?><option value="<?php echo $value['cdid'];?>"><?php echo $value['titel'];?></option><?php } ?></select>  
			</div>
			<div id="minifooter">
				<input type="submit" value="Verwijder product" onclick="return confirm('Are you sure?');">
			</form>	 
			</div>
		</div>
		<?php
		
	}
	else
	{
		echo "U heeft geen toegang tot deze pagina! ";
		echo "<a href='inlogpagina'>login</a>";
	}
}
else
{	
$sqlprod="SELECT titel, cdid FROM cdtabel";
$resultprod=mysqli_query($GLOBALS["con"], $sqlprod)  or die(mysqli_error($GLOBALS["con"]));

?>

<aside>
	<a href="Productentoevoegenverkoper"><div id="pt" class="buttons">Verkopen!</div></a>
	<a href="productkiezen"><div id="pk" class="buttons">Producten wijzigen</div></a>
	<a href="productkiezenverwijder"><div id="pv" class="buttons">cd verwijderen</div></a>
</aside>
<div id="admincontainer">
	<div id="titel">
		Producten verwijderen
	</div>
	<div id="filter">
		<h4>kies het product dat je wilt verwijderen</h4>
	</div>
	<div id="orders">
		<form id="kiesform" name="productkiezen" method="POST" action="productverwijder.php" >
		Product: <select name= "productgekozen" id="productselect"required>
		<?php foreach($resultprod as $value){?><option value="<?php echo $value['cdid'];?>"><?php echo $value['titel'];?></option><?php } ?></select>  
	</div>
	<div id="minifooter">
		<input type="submit" value="Verwijder product" onclick="return confirm('Are you sure?');">
	</form>	 
	</div>
</div>

<?php } ?>
</div>
</div>
<?php
include "include/footer.php";
?>
</body>
</html>