<?php

include_once("config.php");
include_once("include/paginate.php");

include "include/header.php";
error_reporting(-1);
ini_set('display_errors', 'On');
		//current URL of the Page. cart_update.php redirects back to this URL
		$current_url = base64_encode($url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);

		$item_per_page = 2; //item to display per page

	if(isset($_GET["page"])){ //Get page number from $_GET["page"]
		$page_number = filter_var($_GET["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
		if(!is_numeric($page_number) || $page_number < 1){/*die('Invalid page number!');*/ $page_number = 1;} //incase of invalid page number
	}else{
		$page_number = 1; //if there's no page number, set it to 1
	}

	$total = $mysqli->query("SELECT COUNT(cdid) FROM cdtabel");
	$get_total_rows = $total->fetch_row(); //hold total records in variable
	$total_pages = ceil($get_total_rows[0]/$item_per_page); //break records into pages

	$page_position = (($page_number-1) * $item_per_page); //get starting position to fetch the records
?>
<!--Product filter-->
	<div class="row" id='filterproducten'>
		<ul id='filter-wrapper'>
			<li><h4 style='color: white;'>FILTER</h4></li>
				<br /><!--BRRRRR! HET KOUD Hé?-->
				<ul id='result'>
					<li><a href="productpagina.php">Alles</a></li>
<?php
							$result1 = $mysqli->query("SELECT * FROM genre");
									while($row = mysqli_fetch_array($result1)) {
									echo "<li><a href='productpagina.php?category_id=".$row['genreid']." '>". $row['genre'] ."</a></li>";
									}
?>
				</ul>
		</ul>
	</div>
<!--/Product filter-->
<?php
if(isset($_GET['category_id']))
{
	$cID = $_GET['category_id'];
	$results = $mysqli->query("SELECT products.ProductID, products.ProductName, products.ProductOnStock, products.ProductPrice, products.ProductText, products.CategoryID,  media.MediaID, media.ImageURL1 
			FROM products JOIN media ON products.MediaID=media.MediaID WHERE products.CategoryID= $cID ORDER BY ProductID");
}
else
{
	$results = $mysqli->query("SELECT cdtabel.cdid, cdtabel.titel, cdtabel.beschrijving, cdtabel.prijs, cdtabel.afbeelding FROM cdtabel ORDER BY cdid ASC LIMIT $page_position, $item_per_page");
}
?>
<div class="container" style="min-height: 715px;">
    <div class="row">
<?php 
	if($results){
		while($obj = $results->fetch_object()){
			echo '<form method="post" action="cart_update.php">';
?>
    	<div id="test" class="row  col-xs-12 col-sm-5 col-md-3">
			<div class="">
				<div class="thumbnail" >
					<?php echo'<a href="detail.php?art_id=' .  $obj->cdid . '"><h4 class="text-center"><span id="label1" class="label label-info">'.$obj->titel.'</span></h4></a>'; ?>
					<?php echo'<a href="detail.php?art_id=' .  $obj->cdid . '"><img src="img/'.($obj->afbeelding == null ? "noimg.gif" : $obj->afbeelding).'" class="productpage_img"></a>'; ?>
					<div class="caption">
						<div class="row">
							<div class="col-md-6 col-xs-6 price">
								<h3><label><?php echo $currency.$obj->prijs; ?></label></h3>
							</div>
						</div>	
						<div class="row">
							<div class="col-md-6" id="aantal">
								Aantal: <input type="number" class="form-control" name="product_qty" value="1" min="1" max="<?php echo $obj->ProductOnStock;?>" />
							</div>
							<div class="col-md-6">
								kwaliteit: <?php //if($obj->ProductOnStock > 0){ echo $obj->ProductOnStock;} else{ echo "0";} ?>
								<meter value="<?php echo $obj->ProductOnStock; ?>" min="0" max="10"></meter>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
<?php 
/*
* Kunnen we dit nog ergens voor gebruiken?
								if($obj->ProductOnStock > 0){
									echo '<button type="buton" class="btn btn-primary">In winkelwagen</button>';
								}
								else{
									echo '<div class="cartfull">Niet op voorraad!</div>';
								}
*/
?>
							</div>
						</div>
					</div>
				</div>
        	</div> 
		</div>
<?php
			echo '<input type="hidden" name="product_code" value="'.$obj->cdid.'" />';
			echo '<input type="hidden" name="type" value="add" />';
			echo '<input type="hidden" name="return_url" value="'.$current_url.'" />';
			echo '</form>';
		}
	} 
?>
	</div>

<?php if(!isset($_GET['category_id'])) {echo paginate($item_per_page, $page_number, $total_pages);} 
?>

</div><!--/container-->
<?php
	include "include/footer.php";
?>
</body>
</html>