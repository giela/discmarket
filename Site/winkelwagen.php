<?php
include_once("config.php");
include "include/header.php";
?>
<div id="container" class="col-md-12">
	<div id="products-wrapper">
	<h1>Winkelwagen</h1>
	<div class="view-cart">
<?php
	    $current_url = base64_encode($url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
		if(isset($_SESSION["products"]))
	    {
		    $total = 0;
			echo '<form>';
			echo '<div><!--cart-all-->';
			$cart_items = 0;
			foreach ($_SESSION["products"] as $cart_itm)
	        {
	           $product_code = $cart_itm["ProductID"];
			   $results = $mysqli->query("SELECT cdid, titel, prijs, beschrijving FROM cdtabel WHERE cdid='$product_code' LIMIT 1");
			   $obj = $results->fetch_object();
			   
			    echo '<div class="cart-itm" style="border: 1px dotted">';
				echo '<a href="cart_update.php?removep='.$cart_itm["ProductID"].'&return_url='.$current_url.'"><span class="glyphicon glyphicon-remove pull-right"></span></a>';
	            echo '<div class="pull-right">'.$currency.$obj->prijs.'</div>';
	            echo '<div class="product-info">';
				echo '<h3>'.$obj->titel.' (Productnr :'.$product_code.')</h3> ';
				echo '</div><!--/product-info-->';
	            echo '</div><!--/cart-itm-->';
				$subtotal = $obj->prijs;
				$total = ($total + $subtotal);

				echo '<input type="hidden" name="item_name['.$cart_items.']" value="'.$obj->titel.'" />';
				echo '<input type="hidden" name="item_code['.$cart_items.']" value="'.$product_code.'" />';
				echo '<input type="hidden" name="item_desc['.$cart_items.']" value="'.$obj->beschrijving.'" />';
				$cart_items ++;
				
	        }
	    	echo '</div><!--/cart-all-->';
			echo '<span>';
			echo '<strong>Totaal : '.$currency.$total.'</strong>';
			echo '</span></br>';
			echo '<a href="cart_update.php?emptycart=1&return_url='.$current_url.'"><butto12n type="button" class="btn btn-default">Leeg winkelwagen</button></a>';
			echo '</form>';
			echo '<a href="mailadverteerder.php"><button type="button" class="btn btn-succes">Mail de adverteerder</button></a>';
	    }else{
			echo 'De winkelwagen is leeg.';
		}
		
?>
		</div><!--/view-cart-->
	</div>
	<div class="push">  </div><!--Waarom geen br?-->
</div>
<?php
include "include/footer.php";	
?>