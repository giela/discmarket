<?php
include "include/header.php";
?>
<div id="container" style="min-height: 770px;">
	<div id="zoekresultaten">


<?php
//collect
if(isset($_POST['search'])) {
  $searchq = $_POST['search'];
  $searchq = preg_replace("#[^0-9a-z ]#i","",$searchq);
  
  $query="SELECT cdtabel.cdid, cdtabel.titel, cdtabel.beschrijving, cdtabel.prijs, cdtabel.afbeelding, cdtabel.kwaliteit, genre ,uitgever, artiest.artiest FROM  cdtabel  JOIN uitgever ON uitgever.uitgeverid=cdtabel.uitgeverid JOIN genre ON genre.genreid=cdtabel.genreid JOIN artiest ON artiest.artiestid=cdtabel.artiestid WHERE
  cdtabel.titel LIKE '%$searchq%' OR artiest.artiest LIKE '%$searchq%'"; 

  $results=mysqli_query($GLOBALS["con"], $query) or die(mysqli_error($GLOBALS["con"]));  
?>
  <div class="row">
  	<div class="row">
  		<div class="col-md-6" id="resultatentekst">
<?php
	$count = mysqli_num_rows($results);
 		if($count == 0){
    	echo'Geen resultaten gevonden';
  }
  elseif ($count == 1){ 
      echo "$count resultaat gevonden voor '$searchq'";
  }
  else{
  	echo "$count resultaten gevonden voor '$searchq'";
  }
?>
  		</div>
    </div>
<?php 
  if($results){
    while($obj = $results->fetch_object()){
      echo '<form method="post" action="cart_update.php" style="margin-left: 50px;">';
?>
      	<?php echo'<a href="detail.php?art_id=' .  $obj->cdid . '" title="'.$obj->artiest." " . $obj->titel.'">';?>
      <div id="test" class="row  col-xs-12 col-sm-5 col-md-2">
      <div class="">
        <div class="thumbnail" >
          <?php echo'<img src="img/'.($obj->afbeelding == null ? "noimg.gif" : $obj->afbeelding).'" class="productpage_img">'; ?>
          <div class="caption">
            <div class="row">
              <div class="col-md-6 col-xs-6 price">
                <h3><label><?php echo $currency.$obj->prijs; ?></label></h3>
              </div>
            </div>  
            <div class="row">
              <div class="col-md-12" style=" white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
              <b><?php echo $obj->titel; ?></b>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12" style=" white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
              <?php echo $obj->artiest; ?>
                </div>
              </div>
            </div>
          </div>
        </div> 
    </div><!--/test-->
<?php
/*
      echo '<input type="hidden" name="product_code" value="' . $obj->cdid . '" />';
      echo '<input type="hidden" name="type" value="add" />';
      echo '<input type="hidden" name="return_url" value="' . $current_url . '" />'; */
      echo '</form>';
    }
  }
}
?>
  </div>
</div><!--/zoekresultaten-->
</div><!--/Container-->

<?php
include "include/footer.php";
?>
</body>
</html>